/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.currencyparser.utils;

/**
 *
 * @author Alexander
 */
public enum MonthsEnum {

    JAN("январь", 1), FEB("февраль", 2),
    MAR("март", 3), APR("апрель", 4),
    MAY("май", 5), JUN("июнь", 6), JUL("июль", 7),
    AUG("август", 8), SEP("сентябрь", 9),
    OCT("октябрь", 10), NOV("ноябрь", 11), DEC("декабрь", 12);
    private final String monthName;
    private final int number;

    MonthsEnum(String monthName, int number) {
        this.monthName = monthName;
        this.number = number;
    }

    public String getName() {
        return this.monthName;
    }

    public int getNumber() {
        return this.number;
    }

    public static Integer getMonthNumber(String monthName) {
        for (MonthsEnum month : MonthsEnum.values()) {
            if (namesEquals(month.getName(), monthName)) {
                return month.number;
            }
        }
        return null;
    }

    private static boolean namesEquals(String name, String monthName) {
        String normalizedFirst = name.toLowerCase().trim();
        String normalizedSecond = monthName.toLowerCase().trim();
        return normalizedFirst.equalsIgnoreCase(normalizedSecond);
    }
}
