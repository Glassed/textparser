/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.currencyparser.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Alexander
 */

@Configuration
@ComponentScan("gq.kiev.currencyparser")
public class Application {
    
}
