/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.currencyparser.textoperators.impl;

import gq.kiev.currencyparser.entities.CurrencyPair;
import gq.kiev.currencyparser.entities.Rate;
import gq.kiev.currencyparser.entities.Scale;
import gq.kiev.currencyparser.textoperators.TextOperator;
import gq.kiev.currencyparser.utils.MonthsEnum;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alexander
 */
@Service
public class TextOperatorImpl implements TextOperator {

    @Override
    public List<Rate> getAllRates(List<File> files) {
        List<Rate> result = new ArrayList<>();
        for (File file : files) {
            List<String> textFromFile = readFile(file);
            int year = getYearFromFile(file);
            String pairName = getPairNameFromFile(file);
            result.addAll(constructRatesList(year, textFromFile, pairName));
        }
        return result;
    }

    @Override
    public List<Rate> getRates(List<File> fileList, LocalDate date, String receivedPair) {
        //todo validate pair correctly here!
        String pair = receivedPair.toUpperCase();
        List<String> textFromFile = getNeededText(fileList, date, pair);
        List<Rate> rates = constructRatesList(date.getYear(), textFromFile, pair);
        return rates;
    }

    //Indian smell here:
    private List<Rate> constructRatesList(int year, List<String> text, String pair) {
        List<Rate> rates = new ArrayList<>();
        int month = -1;
        int day = -1;
        double buy = -1;
        double sale = -1;
        boolean operation = true;
        for (int i = 0; i < text.size(); i++) {
            if (isMonthName(text.get(i))) {
                month = MonthsEnum.getMonthNumber(text.get(i));
                continue;
            } else if (isDayNumber(text.get(i))) {
                day = Integer.parseInt(text.get(i));
                continue;
            } else if (operation) {
                buy = Double.parseDouble(text.get(i));
                operation = !operation;
                continue;
            } else if (!operation) {
                sale = Double.parseDouble(text.get(i));
                operation = !operation;
            }
            LocalDate exactDate = new LocalDate(year, month, day);
            Rate rate = constructRate(exactDate, buy, sale, pair, rates);
            rates.add(rate);
        }
        return rates;
    }

    //todo set pair and scale correctly(using services) here!
    private Rate constructRate(LocalDate resultDate, double buy, double sale, String pair, List<Rate> rates) {
        Rate rate = new Rate();
        rate.setCreatedDate(resultDate);
        rate.setChangedDate(resultDate);
        rate.setBuy(buy);
        rate.setSale(sale);
        rate.setScale(new Scale(1, "DAY"));
        rate.setCurrencypair(new CurrencyPair(pair));
        return rate;
    }

    private boolean isMonthName(String line) {
        Pattern pattern = Pattern.compile("^[А-Я]+");
        return pattern.matcher(line).matches();
    }

    private boolean isDayNumber(String line) {
        Pattern pattern = Pattern.compile("^[1-9, 0]+");
        return pattern.matcher(line).matches();
    }

    private List<String> getNeededText(List<File> fileList, LocalDate date, String pair) {
        File neededFile = null;
        for (File file : fileList) {
            if (containYear(file, date) && containPair(file, pair)) {
                neededFile = file;
            }
        }
        return readFile(neededFile);
    }

    private List<String> readFile(File file) {
        List<String> lines = new ArrayList<>();
        String line;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TextOperatorImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TextOperatorImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex) {
            Logger.getLogger(TextOperatorImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lines;
    }

    private static boolean containYear(File file, LocalDate date) {
        int year = date.getYear();
        String filename = file.getName();
        String strYear = String.valueOf(year);
        return filename.contains(strYear);
    }

    private static boolean containPair(File file, String pair) {
        String normalizedFilename = file.getName().toLowerCase();
        String normalizedPairname = pair.toLowerCase();
        return normalizedFilename.contains(normalizedPairname);
    }

    private static String getPairNameFromFile(File file) {
        final int beginIndex = 0;
        final int endIndex = 6;
        String pairName = file.getName().substring(beginIndex, endIndex);
        return pairName;
    }

    private static int getYearFromFile(File file) {
        final int beginIndex = 7;
        final int endIndex = 11;
        String yearName = file.getName().substring(beginIndex, endIndex);
        Integer result = null;
        try {
            result = Integer.parseInt(yearName);
        } catch (NumberFormatException numberFormatException) {
            throw new RuntimeException("\nна файле " + file.getName()
                    + "\nN блядь P блядь E нахуй!");
        }
        return result;
    }

}
