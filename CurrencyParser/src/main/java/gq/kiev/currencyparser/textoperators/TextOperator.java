/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.currencyparser.textoperators;

import gq.kiev.currencyparser.entities.CurrencyPair;
import gq.kiev.currencyparser.entities.Rate;
import java.io.File;
import java.util.List;
import java.util.Map;
import org.joda.time.LocalDate;

/**
 *
 * @author Alexander
 */
public interface TextOperator {    
    List<Rate> getRates(List<File> list, LocalDate date, String pair);
    public List<Rate> getAllRates(List<File> files);
}
