/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.currencyparser.fileoperators;

import java.io.File;
import java.util.List;

/**
 *
 * @author Alexander
 */
public interface FileFinder {
    void setFileExtension(String fileType);
    List<File> findFiles(String pathName, boolean checkDirs);
}
