/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.currencyparser.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 *
 * @author Alexander
 */
public class CurrencyPair implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String pair;

    public CurrencyPair() {
    }

    public CurrencyPair(Integer id) {
        this.id = id;
    }

    public CurrencyPair(String pair) {
        this.pair = pair;
    }

    public CurrencyPair(Integer id, String pair) {
        this.id = id;
        this.pair = pair;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.pair);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CurrencyPair other = (CurrencyPair) obj;
        if (!Objects.equals(this.pair, other.pair)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CurrencyPair{ id= " + id + ", pair= " + pair + "}";
    }

}
