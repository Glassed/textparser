/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.currencyparser.entities;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Alexander
 */
public class Scale implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;

    private String scale;

    public Scale() {
    }

    public Scale(Integer id) {
        this.id = id;
    }

    public Scale(Integer id, String scale) {
        this.id = id;
        this.scale = scale;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.scale);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Scale other = (Scale) obj;
        if (!Objects.equals(this.scale, other.scale)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Scale{ id= " + id + ", scale= " + scale + " }";
    }

}
