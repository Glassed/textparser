package gq.kiev.currencyparser.main;

import gq.kiev.currencyparser.config.Application;
import gq.kiev.currencyparser.entities.Rate;
import gq.kiev.currencyparser.fileoperators.FileFinder;
import gq.kiev.currencyparser.fileoperators.impl.FileFinderImpl;
import gq.kiev.currencyparser.textoperators.TextOperator;
import java.io.File;
import java.util.List;
import org.joda.time.LocalDate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Alexander
 */
public class Main {

    private static final String PATH = "RealShit";
    private static ApplicationContext ctx = new AnnotationConfigApplicationContext(Application.class);

    private static FileFinder finder = ctx.getBean(FileFinder.class);
    private static TextOperator txtOpr = ctx.getBean(TextOperator.class);

    public static void main(String[] args) {
        List<File> allFiles = finder.findFiles(PATH, true);

        List<Rate> list = txtOpr.getAllRates(allFiles);
        for(Rate rate : list){
            System.out.println(rate);
        }
    }

}
