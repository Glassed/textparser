/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.currencyparser.fileoperators.impl;

import gq.kiev.currencyparser.fileoperators.FileFinder;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alexander
 */
@Service
public class FileFinderImpl implements FileFinder {
    
    private static String defaultFileExt = ".txt";

    private final List<File> fileList = new ArrayList<>();

    @Override
    public void setFileExtension(String ext) {
        FileFinderImpl.defaultFileExt = ext;
    }    
        
    @Override
    public List<File> findFiles(String pathName, boolean checkDirs) {
        List<File> result = new ArrayList<>();
        if (!dirNameIsOk(pathName)) {
            return result;
        }
        fileList.clear();
        return getFiles(new File(pathName), checkDirs);
    }

    private boolean containPair(File file, String pair) {
        String normalizedFilename = file.getName().toLowerCase();
        String normalizedPairname = pair.toLowerCase();
        return normalizedFilename.contains(normalizedPairname);
    }

    private List<File> getFiles(File dir, final boolean dirsAreChecked) {
        for (File file : dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File filename) {
                if (filename.isDirectory()) {
                    return dirsAreChecked;
                } else if (filename.getName().endsWith(defaultFileExt)) {
                    return true;
                }
                return false;
            }
        })) {
            if (file.isDirectory()) {
                getFiles(file, dirsAreChecked);
            } else {
                fileList.add(file);
            }
        }
        return fileList;
    }

    private static boolean dirNameIsOk(String pathName) {
        try {
            File dir = new File(pathName);
            if (dir.isFile()) {
                return false;
            }
        } catch (NullPointerException npe) {
            return false;
        }
        return true;
    }

}
