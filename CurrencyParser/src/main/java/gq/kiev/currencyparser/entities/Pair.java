package gq.kiev.currencyparser.entities;

/**
 * Created by illia on 28.03.16.
 */
public class Pair {
    private Double buy;
    private Double sale;

    public Pair(Double buy, Double sale) {
        this.buy = buy;
        this.sale = sale;
    }

    public Double getBuy() {
        return buy;
    }

    public Double getSale() {
        return sale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair pair = (Pair) o;

        if (buy != null ? !buy.equals(pair.buy) : pair.buy != null) return false;
        return sale != null ? sale.equals(pair.sale) : pair.sale == null;

    }

    @Override
    public int hashCode() {
        int result = buy != null ? buy.hashCode() : 0;
        result = 31 * result + (sale != null ? sale.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "buy=" + buy +
                ", sale=" + sale +
                '}';
    }
}
